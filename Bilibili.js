/*
 * 回傳單檔 flv，目前所能找到的最高畫質，一般和番劇影片都可使用，cid 必填，avid/aid 或 cid 則一必填，結果一致
 * 2021-09-16_17:30 aid 已無用，必填 cid bvid
 * https://api.bilibili.com/x/player/playurl?otype=json&qn=112&avid=${__INITIAL_STATE__.epInfo.aid}&cid=${__INITIAL_STATE__.epInfo.cid}
 * https://api.bilibili.com/x/player/playurl?cid=291074&bvid=BV1Ex411w7GK&qn=64&type=&otype=json&fourk=1&fnver=0&fnval=80&session=0bb2ae9190e2bf0cafca71283effb035
 * 
 * 回傳單檔 flv，目前所能找到的最高畫質，新發現的 api，無法在一般影片頁面中使用
 * 2021-09-16_17:30 必填 cid，新增加 fnval 參數，112 64 時回傳 flv 檔，和舊 api 一致，80 32 16 時回傳 mp4 影音分割檔
 * https://api.bilibili.com/pgc/player/web/playurl?qn=112&avid=${__INITIAL_STATE__.epInfo.aid}&cid=${__INITIAL_STATE__.epInfo.cid}
 * 
 * 
 * 一般影片的影片串流網址會直接寫在頁面上，變數名 __playinfo__，但無法直接使用，
 * 貌似在加載後被刪除，能從 scripts 中獲得，影片串流為 m4s，貌似是影音分割檔
 * 
 * 手機版網頁能直接在元素中找到影片網址
 * document.querySelector('.player-container script:nth-child(2)').innerHTML.match(/readyVideoUrl: *'([^']+)'/)[1]
 * options 下有需要的影片變數，__INITIAL_STATE__也可使用
 * 
 * 重要參數名 aid cid bvid
 * 一般網頁 __INITIAL_STATE__.videoData.aid
 * 番劇網頁 __INITIAL_STATE__.epInfo.aid
 * 手機版 __INITIAL_STATE__.video.viewInfo.aid / options.aid
 * 
 * 2021-09-16_17:30
 * 番劇站發現無法拿到 aid 等變數，檢查後發現，頁面上已經沒有任何相關變數，__INITIAL_STATE__ __playinfo__ 等變數都不見了
 * https://www.bilibili.com/bangumi/play/ep408851
 * https://www.bilibili.com/bangumi/play/ss39024/
 * 番劇頁面有上面兩種網址形式，都是同一影片，但取得變數的 api 參數不同
 * https://api.bilibili.com/pgc/view/web/season?ep_id=408851
 * https://api.bilibili.com/pgc/view/web/season?season_id=39024
 * 
 * 番劇手機版影片 api
 * https://m.bilibili.com/bangumi/play/ep408851
 * https://m.bilibili.com/bangumi/play/ss39024/
 * https://api.bilibili.com/pgc/player/web/playurl/html5?ep_id=408841&bsource=
 * 
 * https://api.bilibili.com/x/player/pagelist?bvid=BV1Ex411w7GK&jsonp=jsonp
 * 
 * 番劇字幕 api，aid / bvid 則一，cid 必填
 * https://api.bilibili.com/x/player/v2?aid=419893636&cid=391501269&bvid=BV15341167wy
 * 
 * 現在番劇頁面上竟然有 __playinfo__ 裡面有全部的 m4s 檔案來源
 * 也有 __INITIAL_STATE__ 但這些變數在 window 下都找不到，看了下代碼，
 * __INITIAL_STATE__ 會被存入到 md 中，但後續又修改了資料，
 * 目前決定直接從原始碼抓，減少發生異變的可能性
 * 
 * 2021-09-16_10:30
 * 頁面變數已恢復正常，持續觀察中
 * 
 * 獲取影片資訊 cid，必填 aid bvid，一般影片直接回傳所有分頁資料，番劇因為每一集 aid bvid cid 都不同，所以只會拿到一個，
 * 還能拿到番劇字幕網址，包含各種語系
 * https://api.bilibili.com/x/web-interface/view?bvid=BV1SV411W7fD
 */

javascript: (async (defaultQualityCode) => {
  let xhrFetch = (()=>{
    if (isNativeCode('fetch')) {
      return baseFetch
    } else if (isNativeCode('XMLHttpRequest')) {
      return baseXhr
    } else {
      alert('fetch, xhr 皆被修改過')
      return
    }
  
    function baseFetch (url, options = {}) {
      let resType = options.resType
      delete options.resType
  
      return fetch(url, options)
        .then(res => res.text())
        .then(text => convertType(text, resType))
    }
  
    function baseXhr (url, options = {}) {
      let defaultOptions = {
        method: 'GET',
        headers: {},
      }
      options = { ...defaultOptions, ...options }
    
      return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest()
        xhr.open(options.method, url, true)
        // 設定 headers
        for (let args of Object.entries(options.headers)) {
          xhr.setRequestHeader(...args)
        }
        xhr.withCredentials = (options.credentials === 'include')
        xhr.onload = function () { 
          if ([200, 304].includes(this.status)) {
            resolve(convertType(this.response))
          } else {
            reject(this.response)
          }
        }
        xhr.onerror = function () {
          reject(this.response)
        }
        xhr.send(options.body)
      })
    }
  
    function isNativeCode (fnName) {
      return `function ${fnName}() { [native code] }` === eval(`${fnName}.toString()`)
    }
  
    function convertType (text, type) {
      switch (type ?? getResType(text)) {
        case 'json':
          return JSON.parse(text)
  
        case 'doc':
          return new DOMParser().parseFromString(text, 'text/html')
  
        case 'text':
        default:
          return text
      }
    }
  
    function getResType (text) {
      let firstChar = text.match(/[^ \t\n]/)[0]
      switch (firstChar) {
        case '{':
          return 'json'
  
        case '<':
          return 'doc'
  
        default:
          return
      }
    }
  })()
  // 影片格式數據
  const qualityList = Object.freeze([
    {
      code: 120,
      format: 'flv',
      quality: '4K flv',
    },
    {
      code: 116,
      format: 'flv',
      quality: '1080P60 flv',
    },
    {
      code: 112,
      format: 'flv',
      quality: '1080P+ flv',
    },
    {
      code: 80,
      format: 'flv',
      quality: '1080P flv',
    },
    {
      code: 74,
      format: 'flv',
      quality: '720P60 flv',
    },
    {
      code: 64,
      format: 'flv',
      quality: '720P flv',
    },
    {
      code: 32,
      format: 'flv',
      quality: '480P flv',
    },
    {
      code: 16,
      format: 'mp4',
      quality: '360P mp4',
    },
  ])
  let targetQuality = prompt(
    qualityList.map((item, index) => `${index}: ${item.quality}`).join('\n'),
    defaultQualityCode
  )
  if (targetQuality === null) return

  let aid = __INITIAL_STATE__.videoData?.aid ?? __INITIAL_STATE__.epInfo?.aid ?? options.aid
  let bvid = __INITIAL_STATE__.videoData?.bvid ?? __INITIAL_STATE__.epInfo?.bvid ?? options.bvid
  let cid = __INITIAL_STATE__.videoData?.pages?.[__INITIAL_STATE__.p - 1]?.cid ?? __INITIAL_STATE__.epInfo?.cid ?? options.cid
  if ([aid, bvid, cid].includes(undefined)) {
    alert(
      `缺少必要參數\n` +
      `aid: ${aid}\n` +
      `bvid: ${bvid}\n` +
      `cid: ${cid}`
    )
    return
  }
  let apiUrl = `https://api.bilibili.com/x/player/playurl?otype=json&qn=${qualityList[targetQuality].code}&avid=${aid}&bvid=${bvid}&cid=${cid}`
  let res = await xhrFetch(apiUrl, {
    credentials: 'include',
  })
  // 檢查 durl 是否有兩個以上，合理懷疑 flv 也有需要多檔合併的
  if (res.data.durl.length > 1) {
    alert('durl 參數有一個以上，可能需要多檔合併')
    open(apiUrl)
    return
  }
  
  let videoInfo = {
    // format: res.data.format,
    url: res.data.durl[0].url,
    quality: qualityList.find(item => item.code === res.data.quality).quality
  }

  let downloadMessage = qualityList[targetQuality].quality === videoInfo.quality ?
    `${videoInfo.quality}` :
    `${qualityList[targetQuality].quality} => ${videoInfo.quality}`

  if (prompt(downloadMessage, videoInfo.url) !== null) {
    open(videoInfo.url)
  }
})(0)
