javascript: (function() {
  // https://player.vimeo.com/video/580799106/config?autopause=1&byline=0&collections=1&context=Vimeo%5CController%5CClipController.main&default_to_hd=1&h=a8eb717af9&outro=nothing&portrait=0&share=1&speed=0&title=0&watch_trailer=0&s=9cea526eea2ff8f5b1000132ea5ddcbfd4e889a4_1631966845
  // vimeo.clips[_gtm[0].clip_id].request.files.progressive
  let videoList = vimeo.clips?.[_gtm?.[0]?.clip_id]?.request?.files?.progressive
  if (videoList === undefined) {
    alert('progressive 變數不見了')
    return
  }
  videoList.sort((a, b) => b.height - a.height)
  let index = prompt('選擇畫質\n' + videoList.map((item, index) => `${index}: ${item.quality}`).join('\n'), '0')
  if (index !== null && prompt('下載', videoList[index].url)) open(videoList[index].url)
})()