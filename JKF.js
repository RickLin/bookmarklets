javascript: (() => {
  let path = 'F:';
  let dir = document.title.replace(/\\|\/|:|\*|\?|\"|<|>|\|/g, '');
  let imgList = Array.from(document.querySelectorAll('img[id^=aimg_]'));
  let imgUrlList = imgList.map(img => img.getAttribute('file'));

  let content = imgUrlList
    .map((url, i) => `${url}\r\n\tout=${i.toString().padStart(4, '0')}.${url.match(/\.(\w+)$/)[1]}\r\n\tdir=${path}\\${dir}`)
    .join('\r\n');
  let file = new Blob([content], {type: 'text/plain'});

  let downloadButton = document.createElement('a');
  downloadButton.href = URL.createObjectURL(file);
  downloadButton.download = dir + '.lst';
  document.body.append(downloadButton);
  downloadButton.click();
})();
