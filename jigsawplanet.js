/*************************************************************************
網站 localStorage 儲存範例
SG_214486753f7e: 1630516583;{"gametime":164202,"progress":100,"data":"Q0cGBAYAAAKBag=="}
SG_214486753f7e: 1630572701;{gametime: 123994, progress: 100, data: "Q0cGBAYAAAHkWg=="}

SG_214486753f7e: SG_ + 拼圖 ID
1630572701: 時間戳(猜測但不一樣，不影響結果)
gametime: 拼圖完成所用的時間，但不影響畫面顯示
progress: 完成進度，貌似也不影響畫面顯示
data: 數字利用字元編碼儲存的 base64，依序為
C G 03 02 01 00 00 02 94 60
C G 橫排張數 綜排張數 拼圖類型 是否旋轉 完成用時(4個字元，8位16進制數字)
**************************************************************************/

javascript: (() => {
  function getRandomInt (end, start = 0) {
    return Math.floor(Math.random() * (end - start)) + start
  }

  let puzzle = JSON.parse(Array.from(document.scripts).find(js => js.innerHTML.includes('putPuzzle')).innerHTML.match(/\{[^{]+?\}/)[0])
  let customTime = parseInt(prompt('分鐘', '0'), 10)
  let gameTime = customTime === 0 ? getRandomInt(600000, 300000) : (customTime * 60 * 1000) + getRandomInt(60000)
  let data = btoa('CG' +
    String.fromCharCode(
      puzzle.puzzleNx,
      puzzle.puzzleNy,
      puzzle.puzzleCurve,
      puzzle.puzzleRotation ? 1 : 0,
      (gameTime >> 24) & 0xFF,
      (gameTime >> 16) & 0xFF,
      (gameTime >> 8) & 0xFF,
      (gameTime >> 0) & 0xFF,
    ))
  let name = 'SG_' + puzzle.puzzleId
  let value = new Date().getTime().toString().slice(0, -3) + ';' + JSON.stringify({ gametime: gameTime, progress: 100, data })

  window.addEventListener('unload', () => {
    localStorage.setItem(name, value)
  })

  location.reload()
})()
