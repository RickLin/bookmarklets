javascript: (() => {
  fetch(location.href)
    .then(res => res.text())
    .then(text => new DOMParser().parseFromString(text, 'text/html'))
    .then(doc => {
      for (let js of doc.querySelectorAll('script'))
        if ('window.__additionalDataLoaded(' === js.innerHTML.slice(0, 30)) {
          let postInfo = JSON.parse(js.innerHTML.slice(js.innerHTML.indexOf('{'), -2))
          let mediaInfo = postInfo.items[0]
          console.log(mediaInfo)
          switch (mediaInfo.media_type) {
            case 2:
              console.log(mediaInfo.video_versions.reduce((result, node) => node.height >= result.height ? node : result).url)
              break

            case 8:
              console.log(
                mediaInfo.carousel_media
                .filter(node => node.media_type === 2)
                .map(node => node.video_versions.reduce((result, versions) => versions.height >= result.height ? versions : result).url)
                .join('\n')
              )
              break
          }
          break
        }
    })
})()
