/* 
 * // base.js
 * ytcfg.data_.WEB_PLAYER_CONTEXT_CONFIGS.WEB_PLAYER_CONTEXT_CONFIG_ID_KEVLAR_WATCH.jsUrl
 * ytcfg.data_.PLAYER_JS_URL
 * document.querySelector('#movie_player').getAttribute('data-version')
 * location.search.match(/[?&]v=([^?&]+)/)[1]
 * ytcfg.data_.STS
 * 
 * // mobile
 * yt.config_.PLAYER_JS_URL
 * 
 * await xhrFetch('https://m.youtube.com/watch?v=wZpJ0qELfR0&pbj=1', {
 *   headers: {
 *     'x-youtube-client-name': '2',
 *     'x-youtube-client-version': '2.20210916.06.01',
 *     'x-youtube-identity-token': 'QUFFLUhqa0hqTUp6d1NHWVhyWDQ2Y1JDNUtVSi02dDVWQXw=',
 *   }
 * })
 * 
 * // 影片資訊
 * ytInitialPlayerResponse.streamingData
 * // adaptiveFormats 影音分離的串流
 * // formats 單檔完整的檔案
 * 
 * doc = await xhrFetch('https://www.youtube.com/watch?v=mfYruS61p48', { resType: 'document' })
 * 
 * for (let s of doc.scripts) {
 *   if(/var +ytInitialPlayerResponse *=/.test(s.innerHTML)) {
 *     console.log(JSON.parse(s.innerHTML.match(/var +ytInitialPlayerResponse *= *(\{.+\})/)[1]))
 *   }
 * }
 * 
 * for (let s of doc.scripts) {
 *   let formatCode = s.innerHTML.match(/['"]?adaptiveFormats['"]? *: *(\[.+?\])/)?.[1]
 *   if(formatCode !== undefined) {
 *     let formatList = JSON.parse(formatCode)
 *     console.log(formatList.map(f => `${f.qualityLabel ?? f.quality} ${f.averageBitrate >> 10}kb/s ${f.mimeType.match(/\/(\w+);/)?.[1]}`).join('\n'))
 *     break
 *   }
 * }
 * 
 * ytplayer.bootstrapPlayerResponse.streamingData
 * ytplayer.config.args.raw_player_response.streamingData
 * ytcfg.data_ = yt.config_
 * mobile 沒有 ytcfg 變數
 * 
 * 
 * ytcfg
 * yt
 * _yt_player
 * 
 */

// 利用 JSON.parse 來步進判斷是否已經是一個完整的物件字串，效率極差
// let target = /var +ytInitialPlayerResponse *= */
// let r = /[^}]*?[}]/g
// while (true) {
//   try {
//     r.exec(s)
//     JSON.parse(s.slice(30, r.lastIndex))
//     break
//   } catch (e) {
//     if (r.lastIndex === 0) break
//   }
// }


javascript: (async (defaultItag) => {
  let xhrFetch = (() => {
    if (isNativeCode('fetch')) {
      return baseFetch
    } else if (isNativeCode('XMLHttpRequest')) {
      return baseXhr
    } else {
      alert('fetch, xhr 皆被修改過')
      return
    }
  
    function baseFetch (url, options = {}) {
      let resType = options.resType
      delete options.resType
  
      return fetch(url, options)
        .then(res => res.text())
        .then(text => convertType(text, resType))
    }
  
    function baseXhr (url, options = {}) {
      let defaultOptions = {
        method: 'GET',
        headers: {},
      }
      options = { ...defaultOptions, ...options }
    
      return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest()
        xhr.open(options.method, url, true)
        // 設定 headers
        for (let args of Object.entries(options.headers)) {
          xhr.setRequestHeader(...args)
        }
        xhr.withCredentials = (options.credentials === 'include')
        xhr.onload = function () { 
          if ([200, 304].includes(this.status)) {
            resolve(convertType(this.response))
          } else {
            reject(this.response)
          }
        }
        xhr.onerror = function () {
          reject(this.response)
        }
        xhr.send(options.body)
      })
    }
  
    function isNativeCode (fnName) {
      return `function ${fnName}() { [native code] }` === eval(`${fnName}.toString()`)
    }
  
    function convertType (text, type) {
      switch (type ?? getResType(text)) {
        case 'json':
          return JSON.parse(text)
  
        case 'doc':
          return new DOMParser().parseFromString(text, 'text/html')
  
        case 'text':
        default:
          return text
      }
    }
  
    function getResType (text) {
      let firstChar = text.match(/[^ \t\n]/)[0]
      switch (firstChar) {
        case '{':
          return 'json'
  
        case '<':
          return 'doc'
  
        default:
          return
      }
    }
  })()

  async function getVideoInfo (id) {
    let doc = await xhrFetch(`/watch?v=${id}`)
    let scriptEntries = Object.entries(doc.scripts)
    let jsIndex = localStorage.getItem('jsIndex')
    if (jsIndex !== null && scriptEntries[jsIndex] !== undefined) {
      let jsIndexEntry = scriptEntries.splice(jsIndex, 1)[0]
      scriptEntries.unshift(jsIndexEntry)
    }
  
    for (let [index, js] of scriptEntries) {
      let videoInfoRegExp = /(?:var|let) +ytInitialPlayerResponse *= */g
      let jsCode = js.innerHTML

      for (let r = videoInfoRegExp.exec(jsCode); r !== null; r = videoInfoRegExp.exec(jsCode)) {
        try {
          let videoInfo = eval(`(()=>{return ${jsCode.slice(videoInfoRegExp.lastIndex)}})()`)
          if (videoInfo?.streamingData !== undefined) {
            if (jsIndex !== index) localStorage.setItem('jsIndex', index)
            return videoInfo
          }
        } catch (e) {
          console.log(e)
        }
      }
    }
  }

  async function getDecodeFn () {
    let baseJsUrl = document.querySelector('#movie_player').getAttribute('data-version')
    if (localStorage.getItem('baseJsUrl') !== baseJsUrl) {
      let baseJs = await xhrFetch(baseJsUrl)
      let decodeFnName = baseJs.match(/(\w+)\(decodeURIComponent\(\w+\.s\)\)/)[1]
      let decodeFnCode = baseJs.match(new RegExp(decodeFnName + '=(function\\(\\w+\\)\\{[^}]+\\})'))[1]
      let subDecodeFnName = decodeFnCode.match(/;(\w+)\./)[1]
      let subDecodeFnCode = baseJs.match(new RegExp('var (' + subDecodeFnName + '=\\{[\\w\\W]+?\\});'))[1]
    
      localStorage.setItem('baseJsUrl', baseJsUrl)
      localStorage.setItem('decodeFn', `(function(s){let ${subDecodeFnCode};return (${decodeFnCode})(s);})`)
    }

    return eval(localStorage.getItem('decodeFn'))
  }

  let s = `{
    a: ' " ',
    b: ' \\' ',
    c: ' { ',
    d: ' } ',
    e: ' [ ',
    f: ' ] ',
    g: ' ( ',
    h: ' ) ',
    i: ' "\\' ',
    j: " \\"\\' ",
    k: \` \`,
    k: \`\${}\`,
    k: \`\${\` \`}\`,
    k: \`\${\` \` ? ' ' : \` \`}\`,
    k: \`\${\` \` ? ' ' : \` \`} } {\`,
    k: \`\\$\{\${\` \` ? ' ' : \` \`}\`,
    k: \`\\$\{\${\` \` ? ' ' : \`\${\`\`}\`}\`,
    k: \`\\$\{\${0 ? ' ' : \`\${\`\\\`\`}\`}\`,
    k: \`\\$\{\${1 ? ' ' : \`\${\`\\\`\`}\`}\`,
    f: a/b/g,
    f: /b/g,
    a: /df/g,
    a: (/df/g),
    a: v(/df/g),
    a: () => {return /df/g},
    a: () => {yield /df/g},
    a: () => {return /df/g},
    a: () => {throw /df/g},
    a: () => {_yield /df/g},
    a: () => {_return /df/g},
    a: () => {_throw /df/g},
    a: () => { yield /df/g},
    a: () => { return /df/g},
    a: () => { _throw /df/g},
    a: () => /df/g,
    a: () => b + /df/g,
    a: () => {/df/g},
    a: a[/dfsdf/],
  }`
  // getCodeBlock(s)
  console.log(s.slice(0, getCodeBlock(s)+1))

  function getCodeBlock (code, re, type, filter) {
    // let typeBracesList = {
    //   object: ['{'],
    //   string: ['"', '\'', '`'],
    // }

    // let rightBracesList = {
    //   '(': ')',
    //   '[': ']',
    //   '{': '}',
    //   '"': '"',
    // }

    // 括號有可能會因為 re 匹配到不正確的位置，導致後續的括號無法配對，
    // 這時回傳 undefined，雖然可能搜尋完全部字串，但是因為起始位置導致括號無法配對，
    // 後續不應該像字串一樣直接省略搜尋
    // 字串的部分，目前應該是，只要有搜尋到 undefined，後續就通通都會是 undefined，
    // 因為字串的引號沒有左右之分，只要確認是有校引號就會有作用，
    // 所以當已經搜尋完全部有效引號，還是無法配對的話，代表已經沒有任何剩下符合 re 的字串

    // 往左讀取，確認是否會產生 escape 效果
    let isEscape = function (index) {
      let escapeCount = 0
      while (code[index - escapeCount] === '\\') {
        escapeCount++
      }
      return ((escapeCount & 1) === 1)
    }

    function getString (index) {
      // 找出第一個引號
      let quotesList = ['"', '\'', '`']
      while (!quotesList.includes(code[index]) && index < code.length) {
        index++
      }
      if (index >= code.length) return

      let startIndex = index
      let firstQuotes = code[index]
      switch (firstQuotes) {
        case '"':
        case '\'': {
          // 找出下一個可以配對的有效引號即可(不被反斜線跳脫)
          // 先繼續往後讀取，終止條件為 是引號 且 沒被跳脫，所以整個再做 not 處理
          do {
            index++
          } while (!(code[index] === firstQuotes && !isEscape(index - 1)))
    
          break
        }

        case '`': {
          // 要判斷嵌套的寫法
          let keyCharList = ['`']
          do {
            index++
            switch (code[index]) {
              case '`': {
                // 確認是否為有效引號，排除被跳脫的狀況
                if (!isEscape(index - 1)) {
                  if (keyCharList.at(-1) === '`') {
                    keyCharList.pop()
                  } else {
                    keyCharList.push('`')
                  }
                }
                break
              }
        
              case '{': {
                // 確認是否為關鍵字組 ${ 且 沒有被跳脫
                if (code[index - 1] === '$' && !isEscape(index - 2)) {
                  // 確認是有校的 ${ ，push 進 keyCharList
                  keyCharList.push('{')
                }
                break
              }
        
              case '}': {
                // 確認是否是有校的 }
                // 需要 沒有跳脫 且 有能夠配對的左大括號，不然會被解釋為普通字元
                if (!isEscape(index - 1) && keyCharList.at(-1) === '{') {
                  keyCharList.pop()
                }
                break
              }
            }
          } while (keyCharList.length > 0)
          break
        }
      }

      return code.slice(startIndex + 1, index)
    }

    // ====== main ======

    let result = []

    switch (type) {
      case 'string': {
        while (re.exec(code) !== null) {
          let findStr = getString(re.lastIndex)
          if (findStr === undefined) break
          else result.push(findStr)
        }
        break
      }
    }
      
      


    let leftBracesCount = 0

    for (let i = 0, len = code.length; i < len; i++) {
      switch (code[i]) {
        case '\'':
        case '"': {
          // 找出下一個可以配對的有效引號即可(不被反斜線跳脫)
          // 先繼續往後讀取，終止條件為 是引號 且 沒被跳脫，所以整個再做 not 處理
          let quotes = code[i]
          do {
            i++
          } while (!(code[i] === quotes && !isEscape(i - 1)))
          break
        }

        case '`': {
          // 要判斷嵌套的寫法
          let keyCharList = ['`']
          do {
            i++
            switch (code[i]) {
              case '`':
                // 先確認是否為有效引號
                // 在 `` 裡面，只有跳脫寫法才能寫入有效的 ` 字元
                if (!isEscape(i - 1)) {
                  if (keyCharList.at(-1) === '`') {
                    keyCharList.pop()
                  } else {
                    keyCharList.push('`')
                  }
                }
                break

              case '{':
                // 確認是否為關鍵字組 ${ 且 沒有被跳脫
                if (code[i - 1] === '$' && !isEscape(i - 2)) {
                  // 確認是有校的 ${ ，push 進 keyCharList
                  keyCharList.push('{')
                }
                break

              case '}':
                // 確認是否是有校的 }
                // 需要 沒有跳脫 且 有能夠配對的左大括號，不然會被解釋為普通字元
                if (!isEscape(i - 1) && keyCharList.at(-1) === '{') {
                  keyCharList.pop()
                }
                break
            }
          } while (keyCharList.length > 0)
          break
        }

        case '/': {
          // 確認是否為單行註解
          if (code[i + 1] === '/') {
            i++
            do {
              i++
            } while (code[i] !== '\n')
            break
          // 確認是否為多行註解
          } else if (code[i + 1] === '*') {
            i++
            do {
              i++
            } while (!(code[i] === '*' && code[i + 1] === '/'))
            i++
            break
          } else {
            // 要判斷是除法或是正則表達式
            // 先確認是否是有校的正則表達式開頭
            // 找到 / 左邊第一個不為空白的字元
            let leftSpaceIndex = i
            do {
              leftSpaceIndex--
            } while (code[leftSpaceIndex] === ' ')
            
            // 當這個字元非合法變數字元，可以確定 / 左邊缺少正確的運算子，肯定會是正則表達式
            // 就算是合法字元，如果是保留字，那仍然會是正則表達式
            if (
              /[^\w$]/.test(code[leftSpaceIndex]) ||
              /[^\w$](return|throw|yield)$/.test(code.slice(leftSpaceIndex - 6, leftSpaceIndex + 1))
            ) {
              // 確定是正則表達式後，往後尋找有效可以匹配的 /
              do {
                i++
              } while (!(code[i] === '/' && !isEscape(i - 1)))
            }
            break
          }
        }
          
        // case '(':
        // case '[':
        case '{':
          leftBracesCount++
          break

        // case ')':
        // case ']':
        case '}':
          leftBracesCount--
          if (leftBracesCount === 0) {
            return i
          }
      }
    }
  }

  // == main ==

  let videoId = location.search.match(/[?&]v=([^?&]+)/)[1]
  let videoInfo = await getVideoInfo(videoId)
  let formatList = [...videoInfo.streamingData.adaptiveFormats, ...videoInfo.streamingData.formats]
  
  if (formatList[0]?.signatureCipher !== undefined) {
    let decodeFN = await getDecodeFn()
    for (let info of formatList) {
      let urlParams = info.signatureCipher
        .split('&')
        .reduce((result, str) => {
          let [key, val] = str.split('=')
          result[key] = decodeURIComponent(val)
          return result
        }, {})
      info.url = `${urlParams.url}&${urlParams.sp}=${encodeURIComponent(decodeFN(urlParams.s))}`
    }
  }
  
  if (defaultItag === undefined) {
    let lineStartLen = (formatList[0].qualityLabel ?? formatList[0].quality).length
    for (let info of formatList) {
      console.log(
        (info.qualityLabel ?? info.quality).padStart(lineStartLen, ' ') +
        info.mimeType.match(/\/(\w+);/)?.[1].padStart(6, ' ') +
        ((info.averageBitrate >> 10) + 'kb/s').padStart(10, ' ') +
        ((info.contentLength >> 20) + 'MB').padStart(8, ' ') +
        '\n' +
        info.url
      )
    }
  } else {
    location.href = formatList.find(item => item.itag === defaultItag).url
  }
})()

// ================================================================================

// Youtube 電腦版轉頁 api
async function getVideoInfo (id) {
  return await xhrFetch('https://www.youtube.com/youtubei/v1/player?key=' + ytcfg.data_.INNERTUBE_API_KEY, {
    'body': JSON.stringify({
      'context': ytcfg.data_.INNERTUBE_CONTEXT,
      'videoId': id,
      'playbackContext': {
        'contentPlaybackContext': {
          'signatureTimestamp': ytcfg.data_.STS,
        },
      },
    }),
    'method': 'POST',
  })
}

// 利用 download4 網站
javascript: (async a => {
  let b = function() {
      let a = {
        message: "crawling",
        isRun: !0,
        originalTitle: document.title
      };
      document.title = a.message;
      let b = setInterval(() => {
        document.title.length >= a.message.length + 5 ? document.title = a.message : document.title += "."
      }, 500);
      return () => {
        clearInterval(b), document.title = a.originalTitle
      }
    }(),
    c = await async function(a) {
      return fetch("https://www.download4.cc/media/parse?address=" + a, {
        headers: {
          Accept: "application/x-www-form-urlencoded",
          "Content-Type": "application/x-www-form-urlencoded;charset=utf-8"
        },
        mode: "cors",
        redirect: "follow",
        method: "GET"
      }).then(a => a.json())
    }(location.href);
  b();
  let d = c.data.formats.find(b => b.ext === a).url;
  open(d)
})("m4a");



