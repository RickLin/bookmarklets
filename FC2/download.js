javascript: (function() {
  var id = location.pathname.match(/\d{8}\w{8}/)[0]
  var gk = (typeof getKey) === 'function' ? getKey() : undefined

  function getList(s) {
    let par = {}
    s.replace(/([^&=]+)=([^&=]*)/g, function(i, n, v) {
      par[n] = v
    })
    return par
  }

  function download() {
    let xhr = new XMLHttpRequest()
    xhr.onload = function() {
      let par = getList(xhr.responseText.replace(/&amp;/g, '&'))
      if (par['filepath'] && par['mid']) {
        let URL = par['filepath'] + '?mid=' + par['mid']
        if (prompt('下載網址', URL)) location.href = URL
      } else if (par['err_code'] === '403') {
        alert(par['err_text'])
      } else if (par['err_code'] === '503') {
        alert('err_code=503')
        location.href = xhr.responseURL
      } else if (par['err_code'] === '603') {
        alert('請登入')
      } else {
        alert(xhr.responseText)
      }
    }
    xhr.open('GET', 'https://video.fc2.com/ginfo.php?otag=0&upid=' + id + '&mimi=' + md5(id + '_gGddgPfeaf_gzyr') + (gk ? '&gk=' + gk : ''))
    xhr.send()
  }
  if ((typeof md5) === 'function') {
    download()
  } else {
    let s = document.createElement('script')
    s.type = 'text/javascript'
    s.onload = function() {
      download()
    }
    s.src = 'https://blueimp.github.io/JavaScript-MD5/js/md5.js'
    document.head.appendChild(s)
  }
})()
