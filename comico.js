javascript: (function() {
  let fisrtImg = document.querySelector('.comic-image__image')
  let imgSrcList = JSON.parse(JSON.stringify(cmnData.imageData))
  imgSrcList.unshift(fisrtImg.src)
  let title = document.title.split(' | ')[1].match(/\d+/)[0].padStart(3, '0')
  clean(document.documentElement)
  document.title = title
  document.body.style.display = 'flex'
  document.body.style.flexDirection = 'column'
  document.body.style.alignItems = 'center'
  for (let imgSrc of imgSrcList) {
    let img = new Image()
    img.src = imgSrc
    document.body.appendChild(img)
  }

  function clean(e) {
    Object.values(e.attributes).map(attr => attr.name).forEach(name => e.removeAttribute(name))
    e.innerHTML = ''
  }
})()