javascript: (function() {
  function clean(el) {
    Object.values(el.attributes)
      .map(attr => attr.name)
      .forEach(name => el.removeAttribute(name))
    el.innerHTML = ''
  }

  function decoder (str) {
    return CryptoJS.AES.decrypt(str, CryptoJS.enc.Utf8.parse('a7fc9dc89f2c873d79397f8a0028a4cd'), {
      iv: CryptoJS.enc.Utf8.parse('\0'.repeat(32)),
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7
    }).toString(CryptoJS.enc.Utf8)
  }

  let imgSrcList = images
    .map(item => decoder(item.url).replace(/\/dims\/crop\/[^\/]+\/optimize/, '?') + item.parameter)
    .filter((item, index, arr) => arr.indexOf(item) === index)
  let title = document.querySelector('h1.tit_comic').textContent.match(/\d+/)[0].padStart(3, '0')

  clean(document.documentElement)

  document.title = title
  document.body.style.display = 'flex'
  document.body.style.flexDirection = 'column'
  document.body.style.alignItems = 'center'

  for (let imgSrc of imgSrcList) {
    let img = new Image()
    img.src = imgSrc
    document.body.appendChild(img)
  }
})()

// let imgUrlList = (await fetch("https://api.pocketcomics.com/comic/177/chapter/341/product", {
//   "headers": {
//     "x-comico-check-sum": "d0fec4ce392352440647e7424ba24f6c860b945d223a1251875b54aba74bd9bb",
//     "x-comico-client-immutable-uid": "0.0.0.0",
//     "x-comico-client-os": "other",
//     "x-comico-client-platform": "web",
//     "x-comico-client-store": "other",
//     "x-comico-request-time": new Date().getTime().toString().slice(0, -3),
//     'x-comico-timezone-id': 'Asia/Taipei',
//   },
//   "method": "GET",
//   "credentials": "include"
// })
//   .then(res => res.json())).data.chapter.images
//   .map(item => decoder(item.url).replace(/\/dims\/crop\/[^\/]+\/optimize/, '?') + item.parameter)
//   .filter((item, index, arr) => arr.indexOf(item) === index)